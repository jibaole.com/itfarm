package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class Favorite implements Serializable {
    private Integer recordId;

    private Integer userId;

    private Integer articleId;

    private String articleTitle;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Favorite(Integer recordId, Integer userId, Integer articleId, String articleTitle, Date createTime) {
        this.recordId = recordId;
        this.userId = userId;
        this.articleId = articleId;
        this.articleTitle = articleTitle;
        this.createTime = createTime;
    }

    public Favorite() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getArticleId() {
        return articleId;
    }

    public void setArticleId(Integer articleId) {
        this.articleId = articleId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle == null ? null : articleTitle.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}