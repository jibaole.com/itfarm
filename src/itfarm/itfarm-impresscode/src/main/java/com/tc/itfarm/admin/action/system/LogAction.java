package com.tc.itfarm.admin.action.system;

import com.tc.itfarm.api.common.Codes;
import com.tc.itfarm.api.enums.LogType;
import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.model.Log;
import com.tc.itfarm.service.LogService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * Created by wangdongdong on 2016/9/8.
 */
@Controller
@RequestMapping("/log")
public class LogAction {

    @Resource
    private LogService logService;

    @RequestMapping("/list")
    public String list(Model model, Integer pageNo, Integer type) {
        Page page = new Page(pageNo, Codes.LOG_PAGE_SIZE);
        PageList<Log> logPageList = logService.selectByPage(type == null ? null : LogType.valueOfCode(type).getName(), page, null, null);
        model.addAttribute("list", logPageList.getData());
        model.addAttribute("page", logPageList.getPage());
        model.addAttribute("type", type);
        return "admin/log/list";
    }

}
