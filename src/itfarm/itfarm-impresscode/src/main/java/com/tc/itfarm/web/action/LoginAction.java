package com.tc.itfarm.web.action;

import com.tc.itfarm.api.util.BeanMapper;
import com.tc.itfarm.api.util.EncoderUtil;
import com.tc.itfarm.model.User;
import com.tc.itfarm.service.FavoriteService;
import com.tc.itfarm.service.UserService;
import com.tc.itfarm.web.vo.UserVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by wangdongdong on 2016/8/30.
 */
@Controller
@RequestMapping("/login")
public class LoginAction {

    @Resource
    private UserService userService;
    @Resource
    private FavoriteService favoriteService;

    @RequestMapping("toLogin")
    public String toLogin(HttpServletRequest request, String type) {
        request.setAttribute("type", type);
        return "login/index";
    }

    /**
     * 登录
     * @param user
     * @param request
     * @return
     */
    @RequestMapping("/loginDo")
    public String loginDo(User user, HttpServletRequest request) {
        User u = userService.select(user.getUsername(), EncoderUtil.md5(user.getPassword()));
        if (u != null) {
            UserVO userVO = new UserVO();
            BeanMapper.copy(userVO, u);
            userVO.setFavoriteIds(favoriteService.selectArticleIds(u.getRecordId()));
            request.getSession().setAttribute("user", userVO);
            return "redirect:/article/index.do";
        } else {
            request.setAttribute("error", "用户名或密码错误!");
            return "login/index";
        }
    }

    /**
     * 登出
     * @param request
     * @return
     */
    @RequestMapping("loginOut")
    public String loginOut(HttpServletRequest request) {
        request.getSession().invalidate();
        return "login/index";
    }

    /**
     * 注册
     * @param user
     * @param request
     * @return
     */
    @RequestMapping("/register")
    public String register(User user, HttpServletRequest request) {
        request.setAttribute("type", "reg");
        request.setAttribute("username", user.getUsername());
        request.setAttribute("password", user.getPassword());
        request.setAttribute("email", user.getEmail());
        if (user.getUsername() == null || user.getUsername().trim().equals("")) {
            request.setAttribute("errorMsg", "用户名不能为空!");
            return "login/index";
        }
        if (userService.selectByUsername(user.getUsername()) != null) {
            request.setAttribute("errorMsg", "该用户名已被注册!");
            return "login/index";
        }
        if (user.getEmail() == null || user.getEmail().trim().equals("")) {
            request.setAttribute("errorMsg", "邮箱不能为空!");
            return "login/index";
        }
        if (user.getPassword() == null || user.getPassword().trim().equals("")) {
            request.setAttribute("errorMsg", "密码不能为空!");
            return "login/index";
        }
        user.setStatus(1);
        user.setNickname(user.getUsername());
        userService.save(user);
        request.setAttribute("type", "");
        return "redirect:toLogin.do";
    }
}
