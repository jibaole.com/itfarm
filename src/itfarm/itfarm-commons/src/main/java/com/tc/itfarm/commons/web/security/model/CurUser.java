package com.tc.itfarm.commons.web.security.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
/**
 * 
 * @Description:当前登陆的用户  
 * @author: wangdongdong  
 * @date:   2016年7月13日 上午11:16:45   
 *
 */
public class CurUser extends User{
	
	private static final long serialVersionUID = 1L;
	
	private com.tc.itfarm.model.User curUser;

	public CurUser(String username, String password, boolean enabled, boolean accountNonExpired,
			boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities, com.tc.itfarm.model.User curUser) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.curUser = curUser;
	}

	public com.tc.itfarm.model.User getCurUser() {
		return curUser;
	}

	public void setCurUser(com.tc.itfarm.model.User curUser) {
		this.curUser = curUser;
	}

}
